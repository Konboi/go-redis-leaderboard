package lb

import (
	"log"
	"testing"

	"github.com/soh335/go-test-redisserver"
	"gopkg.in/redis.v4"
)

var (
	testRedisServer *redistest.Server
	testRedisClient *redis.Client
	testKey         = "test-leaderboard"
)

type RankWithScore struct {
	Ms   MemberScore
	Rank int64
}

func init() {
	s, err := redistest.NewServer(true, nil)
	if err != nil {
		log.Fatal(err)
	}

	testRedisServer = s
	testRedisClient = redis.NewClient(&redis.Options{
		Network: "unix",
		Addr:    s.Config["unixsocket"],
	})
	cmd := testRedisClient.Ping()
	_, err = cmd.Result()
	if err != nil {
		log.Fatal(err)
	}
}

func TestGetIncrSet(t *testing.T) {
	lb := NewLeaderBoard(testRedisClient, "test1", "desc")

	err := lb.SetScore(MemberScore{"one", 10})
	if err != nil {
		t.Fatal(err)
	}

	if lb.IncrScore("one", 10) != 20 {
		t.Fatal("error incr score")
	}

	if lb.GetScore("one") != 20 {
		t.Fatal("error get score")
	}

	if lb.DecrScore("one", 10) != 10 {
		t.Fatal("error decr score")
	}

	if lb.GetScore("one") != 10 {
		t.Fatal("error decr score")
	}

	err = lb.SetScore(MemberScore{"one", 5})
	if err != nil {
		t.Fatal(err)
	}

	if lb.GetScore("one") != 5 {
		t.Fatal("error set score")
	}
}

func TestEmpty(t *testing.T) {
	lb := NewLeaderBoard(testRedisClient, "empty", "desc")

	score, rank := lb.GetRankWithScore("one")

	if score != 0 {
		t.Fatal("error empty set score")
	}

	if rank != 0 {
		t.Fatal("error empty rank")
	}

	if lb.GetScore("one") != 0 {
		t.Fatal("error empty score")
	}

	if lb.GetRank("one") != 0 {
		t.Fatal("error empty rank")
	}
}

func TestGetRankWithScoreAsc(t *testing.T) {
	lb := NewLeaderBoard(testRedisClient, "test_asc", "desc")

	rws := []RankWithScore{
		{MemberScore{"one", 100}, 1},
		{MemberScore{"two", 50}, 2},
		{MemberScore{"two2", 50}, 2},
		{MemberScore{"four", 30}, 4},
		{MemberScore{"five", 10}, 5},
		{MemberScore{"six", 8}, 6},
		{MemberScore{"six2", 8}, 6},
		{MemberScore{"six3", 8}, 6},
		{MemberScore{"nine", 1}, 9},
	}
	for _, r := range rws {
		lb.SetScore(MemberScore{r.Ms.Member, r.Ms.Score})
	}

	for _, r := range rws {
		rank, score := lb.GetRankWithScore(r.Ms.Member)

		if score != r.Ms.Score {
			t.Fatalf("[error] %s score is invalid. score will %f but %f", r.Ms.Member, r.Ms.Score, score)
		}

		if rank != r.Rank {
			t.Fatalf("[error] %s rank is invalid. rank will %d but %d", r.Ms.Member, r.Rank, rank)
		}

	}
}

func TestGetRankWithScoreDesc(t *testing.T) {
	lb := NewLeaderBoard(testRedisClient, "test_desc", "asc")

	rws := []RankWithScore{
		{MemberScore{"one", -11}, 1},
		{MemberScore{"two", 11}, 2},
		{MemberScore{"two2", 11}, 2},
		{MemberScore{"four", 20}, 4},
		{MemberScore{"five", 30}, 5},
		{MemberScore{"six", 44}, 6},
		{MemberScore{"six2", 44}, 6},
		{MemberScore{"six3", 44}, 6},
		{MemberScore{"nine", 80}, 9},
	}
	for _, r := range rws {
		lb.SetScore(MemberScore{r.Ms.Member, r.Ms.Score})
	}

	for _, r := range rws {
		rank, score := lb.GetRankWithScore(r.Ms.Member)

		if score != r.Ms.Score {
			t.Fatalf("[error] %s score is invalid. score will %f but %f", r.Ms.Member, r.Ms.Score, score)
		}

		if rank != r.Rank {
			t.Fatalf("[error] %s rank is invalid. rank will %d but %d", r.Ms.Member, r.Rank, rank)
		}
	}
}

func TestGetRankWithScoreSame(t *testing.T) {
	lb := NewLeaderBoard(testRedisClient, "test_same1", "desc")

	rws := []RankWithScore{
		{MemberScore{"one", 100}, 1},
		{MemberScore{"one2", 100}, 1},
		{MemberScore{"three", 50}, 3},
		{MemberScore{"four", 30}, 4},
	}

	for _, r := range rws {
		lb.SetScore(MemberScore{r.Ms.Member, r.Ms.Score})
	}

	rank, score := lb.GetRankWithScore("one")
	if rank != 1 && score != 100 {
		t.Fatal("[error] fail get rank with score")
	}

	rank, score = lb.GetRankWithScore("one2")
	if rank != 1 && score != 100 {
		t.Fatal("[error] fail get rank with score")
	}

	rank, score = lb.GetRankWithScore("three")
	if rank != 3 && score != 50 {
		t.Fatal("[error] fail get rank with score")
	}

	lb.IncrScore("one", 1)

	rank, score = lb.GetRankWithScore("one")
	if rank != 1 && score != 101 {
		t.Fatal("[error] fail get rank with score")
	}

	rank, score = lb.GetRankWithScore("one2")
	if rank != 2 && score != 100 {
		t.Fatal("[error] fail get rank with score")
	}

	if lb.MemberCount("", "") != 4 {
		t.Fatal("[error] fail get member count")
	}

	rankings := lb.Rankings(0, 0)
	if len(rankings) != 4 {
		t.Fatal("[error] invalid ranking data")
	}

	if ranking := rankings[0]; ranking.Ms.Member != "one" || ranking.Ms.Score != 101 || ranking.Rank != 1 {
		t.Fatal("[error] invalid ranking data")
	}

	if ranking := rankings[1]; ranking.Ms.Member != "one2" || ranking.Ms.Score != 100 || ranking.Rank != 2 {
		t.Fatal("[error] invalid ranking data")
	}

	if ranking := rankings[2]; ranking.Ms.Member != "three" || ranking.Ms.Score != 50 || ranking.Rank != 3 {
		t.Fatal("[error] invalid ranking data")
	}

	if ranking := rankings[3]; ranking.Ms.Member != "four" || ranking.Ms.Score != 30 || ranking.Rank != 4 {
		t.Fatal("[error] invalid ranking data")
	}

	rankings = lb.Rankings(2, 0)
	if len(rankings) != 2 {
		t.Fatal("[error] invalid ranking data")
	}

	if ranking := rankings[0]; ranking.Ms.Member != "one" || ranking.Ms.Score != 101 || ranking.Rank != 1 {
		t.Fatal("[error] invalid ranking data")
	}

	if ranking := rankings[1]; ranking.Ms.Member != "one2" || ranking.Ms.Score != 100 || ranking.Rank != 2 {
		t.Fatal("[error] invalid ranking data")
	}

	rankings = lb.Rankings(1, 2)
	if len(rankings) != 1 {
		t.Fatal("[error] invalid ranking data")
	}
	if ranking := rankings[0]; ranking.Ms.Member != "three" || ranking.Ms.Score != 50 || ranking.Rank != 3 {
		t.Fatal("[error] invalid ranking data")
	}

	rankings = lb.Rankings(10, 2)
	if len(rankings) != 2 {
		t.Fatal("[error] invalid ranking data")
	}
	if ranking := rankings[0]; ranking.Ms.Member != "three" || ranking.Ms.Score != 50 || ranking.Rank != 3 {
		t.Fatal("[error] invalid ranking data")
	}
	if ranking := rankings[1]; ranking.Ms.Member != "four" || ranking.Ms.Score != 30 || ranking.Rank != 4 {
		t.Fatal("[error] invalid ranking data")
	}

	lb.Remove("one2")
	rank, score = lb.GetRankWithScore("three")
	if rank != 2 || score != 50 {
		t.Fatal("[error] invalid data", rank, score)
	}
}
