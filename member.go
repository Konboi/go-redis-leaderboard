package lb

type Member struct {
	Member string
	lb     LeaderBoard
}

func NewMember(lb LeaderBoard, member string) *Member {
	return &Member{
		Member: member,
		lb:     lb,
	}
}

func (m *Member) Score() float64 {
	return m.lb.GetScore(m.Member)
}

func (m *Member) SetScore(score float64) float64 {
	m.lb.SetScore(MemberScore{m.Member, score})

	return score

}

func (m *Member) Incr(score float64) float64 {
	return m.lb.IncrScore(m.Member, score)
}

func (m *Member) Decr(score float64) float64 {
	return m.lb.DecrScore(m.Member, score)
}

func (m *Member) RankWithScore(score float64) (int64, float64) {
	return m.lb.GetRankWithScore(m.Member)
}

func (m *Member) Rank() (rank int64) {
	return m.lb.GetRank(m.Member)
}

func (m *Member) SortedOrder() (rank int64) {
	return m.lb.GetSortedOrder(m.Member)
}
