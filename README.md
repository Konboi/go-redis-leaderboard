# go-redis-leaderboard


leaderboard using redis. Inspired by [Redis::Leaderboard](https://github.com/Songmu/p5-Redis-LeaderBoard)

# description

It is private study project for me and still alpha quority. API will be changed.

# useage

```
package main

import (
    "fmt"

    "github.com/Konboi/go-redis-leaderboard"
)

func main () {
    lb := NewLeaderBoard(RedisClient, Key, "desc")
    lb.SetScore{MemberScore{"user1", 100}}
    lb.SetScore{MemberScore{"user2", 50}}

    fmt.Println("user1 rank is", lb.GetRank("user1"))
}
```
