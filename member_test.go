package lb

import (
	"log"
	"testing"

	"github.com/soh335/go-test-redisserver"
	"gopkg.in/redis.v4"
)

func init() {
	s, err := redistest.NewServer(true, nil)
	if err != nil {
		log.Fatal(err)
	}

	testRedisServer = s
	testRedisClient = redis.NewClient(&redis.Options{
		Network: "unix",
		Addr:    s.Config["unixsocket"],
	})
	cmd := testRedisClient.Ping()
	_, err = cmd.Result()
	if err != nil {
		log.Fatal(err)
	}
}

func TestGetRankWithScore(t *testing.T) {
	lb := NewLeaderBoard(testRedisClient, testKey, "desc")

	rws := []RankWithScore{
		{MemberScore{"one", 100}, 1},
		{MemberScore{"two", 50}, 2},
		{MemberScore{"two2", 50}, 2},
		{MemberScore{"four", 30}, 4},
		{MemberScore{"five", 10}, 5},
		{MemberScore{"six", 8}, 6},
		{MemberScore{"six2", 8}, 6},
		{MemberScore{"six3", 8}, 6},
		{MemberScore{"nine", 1}, 9},
	}

	for _, r := range rws {
		lb.SetScore(MemberScore{r.Ms.Member, r.Ms.Score})
	}

	m := lb.FindMember("six")
	if m.Rank() != 6 || m.Score() != 8 {
		t.Fatal("[error] get member data")
	}

	m.Decr(1)
	if m.Rank() != 8 || m.Score() != 7 {
		t.Fatal("[error] decr member data")
	}

	m.Incr(3)
	if m.Rank() != 5 || m.Score() != 10 {
		t.Fatal("[error] incr member data")
	}
	if lb.GetRank(m.Member) != 5 {
		t.Fatal("[error] get ranking data")
	}

	m.SetScore(110)
	if m.Rank() != 1 {
		t.Fatal("[error] set user score")
	}
}
