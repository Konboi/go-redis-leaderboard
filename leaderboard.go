package lb

import (
	"fmt"

	"gopkg.in/redis.v4"
)

type LeaderBoard struct {
	Key      string
	Redis    *redis.Client
	Order    string
	ExPireAt int
}

type MemberScore struct {
	Member string
	Score  float64
}

type Ranking struct {
	Ms   MemberScore
	Rank int64
}

func NewLeaderBoard(redis *redis.Client, key string, order string) *LeaderBoard {
	return &LeaderBoard{
		Key:   key,
		Redis: redis,
		Order: order,
	}
}

func (lb *LeaderBoard) IsAsc() bool {
	if lb.Order == "asc" {
		return true
	}

	return false
}

func (lb *LeaderBoard) SetScore(mss ...MemberScore) error {
	var z []redis.Z
	for _, ms := range mss {
		z = append(z, redis.Z{
			Score:  ms.Score,
			Member: ms.Member,
		})
	}

	cmd := lb.Redis.ZAdd(lb.Key, z...)
	_, err := cmd.Result()

	return err
}

func (lb *LeaderBoard) IncrScore(member string, score float64) float64 {
	cmd := lb.Redis.ZIncrBy(lb.Key, score, member)

	return cmd.Val()
}

func (lb *LeaderBoard) DecrScore(member string, score float64) float64 {
	cmd := lb.Redis.ZIncrBy(lb.Key, -(score), member)

	return cmd.Val()
}

func (lb *LeaderBoard) GetScore(member string) float64 {
	cmd := lb.Redis.ZScore(lb.Key, member)

	return cmd.Val()
}

func (lb *LeaderBoard) GetRank(member string) int64 {
	rank, _ := lb.GetRankWithScore(member)

	return rank
}

func (lb *LeaderBoard) GetRankWithScore(member string) (rank int64, score float64) {
	score = lb.GetScore(member)
	rank = 0

	if score == 0 {
		return rank, score
	}

	if lb.IsAsc() {
		cmd := lb.Redis.ZRank(lb.Key, member)
		rank = cmd.Val()
	} else {
		cmd := lb.Redis.ZRevRank(lb.Key, member)
		rank = cmd.Val()
	}

	if rank == 0 {
		return 1, score
	}

	var min, max string
	if lb.IsAsc() {
		min, max = "-inf", fmt.Sprintf("(%f", score)
	} else {
		min, max = fmt.Sprintf("(%f", score), "inf"
	}

	cnt := lb.MemberCount(min, max)
	rank = cnt + 1

	return rank, score
}

func (lb *LeaderBoard) MemberCount(from, to string) int64 {
	if from == "" && to == "" {
		cmd := lb.Redis.ZCard(lb.Key)
		return cmd.Val()
	}

	if from == "" {
		from = "-inf"
	}

	if to == "" {
		to = "inf"
	}
	cmd := lb.Redis.ZCount(lb.Key, from, to)

	return cmd.Val()
}

func (lb *LeaderBoard) Rankings(limit int64, offset int64) []Ranking {
	if limit == 0 {
		limit = lb.MemberCount("", "")
	}

	var members []string
	if lb.IsAsc() {
		cmd := lb.Redis.ZRange(lb.Key, offset, offset+limit-1)
		members = cmd.Val()
	} else {
		cmd := lb.Redis.ZRevRange(lb.Key, offset, offset+limit-1)
		members = cmd.Val()
	}

	var rankings []Ranking
	for _, m := range members {
		rank, score := lb.GetRankWithScore(m)
		rankings = append(rankings, Ranking{MemberScore{m, score}, rank})
	}

	return rankings
}

func (lb *LeaderBoard) Remove(members ...string) {
	ms := []interface{}{}
	for _, m := range members {
		ms = append(ms, m)
	}

	lb.Redis.ZRem(lb.Key, ms...)
}

func (lb *LeaderBoard) GetSortedOrder(member string) int64 {
	var rank int64

	if lb.IsAsc() {
		cmd := lb.Redis.ZRank(lb.Key, member)
		rank = cmd.Val()
	} else {
		cmd := lb.Redis.ZRevRank(lb.Key, member)
		rank = cmd.Val()
	}

	return rank
}

func (lb *LeaderBoard) FindMember(member string) *Member {
	return NewMember(*lb, member)
}
